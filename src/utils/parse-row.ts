import type { Row } from "../types/row.type";

function replaceSpaces(text: string): string {
  return text.replaceAll("_", " ");
}

export function parseRow(rows: string[]): Row[] {
  return rows.map((row) => {
    const [number, fullname, start, finish, club] = row.split(" ");
    return {
      number,
      fullname: replaceSpaces(fullname),
      start,
      finish,
      club,
    };
  });
}
