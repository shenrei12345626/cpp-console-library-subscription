export function differenceBeetwenTimesInMinutes(from: number, to: number) {
  return differenceBeetwenTimesInSeconds(from, to) / 60;
}

export function differenceBeetwenTimesInSeconds(from: number, to: number) {

  return to - from;
}
