import { differenceBeetwenTimesInSeconds } from "../utils/difference-beetwen-times";
import { hmsToSeconds } from "../utils/hms-to-secords";

import type { Row } from "../types/row.type";

export function betterTime(rows: Row[], minutes: number) {
  const runs: string[] = [];

  rows.forEach(({ number, fullname, start, finish, club  }) => {
    const difference = differenceBeetwenTimesInSeconds(
      hmsToSeconds(start),
      hmsToSeconds(finish)
    );

    if (difference < minutes || difference === minutes) {
      runs.push(fullname, club);
    }
  });

  return runs;
}
