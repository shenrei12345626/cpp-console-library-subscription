import type { Row } from "../types/row.type";

export function spartak(rows: Row[], name: string) {
  const runs: string[] = [];

  rows.forEach(({ fullname, start, finish, club}) => {
    if (club.includes(name)) {
      runs.push(fullname, "старт: "+ start, "финиш: "+ finish);
    }
  });

  return runs;
}
