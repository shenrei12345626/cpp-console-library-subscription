import { hmsToSeconds } from "../utils/hms-to-secords";
import { secondsToHMS } from "../utils/seconds-to-hms";
import { differenceBeetwenTimesInSeconds } from "../utils/difference-beetwen-times";

import type { Row } from "../types/row.type";

export function betterPeople(rows: Row[]): [Row, string] {
  const maxRow = rows.reduce<Row>((accamulator, row) => {
    const currentSecondsFrom = hmsToSeconds(row.start);
    const currentSecondsTo = hmsToSeconds(row.finish);
    const previousSecondsFrom = hmsToSeconds(accamulator.start);
    const previousSecondsTo = hmsToSeconds(accamulator.finish);

    const currentDifference = differenceBeetwenTimesInSeconds(
      currentSecondsFrom,
      currentSecondsTo
    );
    const previousDifference = differenceBeetwenTimesInSeconds(
      previousSecondsFrom,
      previousSecondsTo
    );

    if (currentDifference < previousDifference) {
      return row;
    }

    return accamulator;
  }, rows[0]);

  return [
    maxRow,
    secondsToHMS(
      differenceBeetwenTimesInSeconds(
        hmsToSeconds(maxRow.start),
        hmsToSeconds(maxRow.finish)
      )
    ),
  ];
}
