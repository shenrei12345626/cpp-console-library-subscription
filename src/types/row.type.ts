export type Row = {
  number: string;
  fullname: string;
  start: string;
  finish: string;
  club: string;
};
