import { readFileLines } from "./utils/read-file-lines";
import { parseRow } from "./utils/parse-row";
import { spartak } from "./tasks/spartak";
import { betterTime } from "./tasks/better-then";
import { betterPeople } from "./tasks/min-time";

readFileLines("./data.txt", parseRow).then((rows) => {
  const spartak_find = spartak(rows, "Спартак");
  const better = betterTime(rows, 10200);

  const [{ club, fullname }, better_people] = betterPeople(rows);

  console.log("Клуб Спартак", spartak_find);
  console.log("Результат лучше 2:50:00:", better);

  console.log(
    "Самый лучший результат:", club, "-", fullname, "/", better_people
  );
});
